angular.module('checkoutController',[])

.controller('checkoutCtrl',['Checkout','$scope','$rootScope',function (Checkout,$scope,$rootScope) {
    const app = this;
    app.sampath = false;
    app.dialog = false;
    app.orderingData = JSON.parse(localStorage.getItem('orderingData'));
    app.subtotal = JSON.parse(localStorage.getItem('finalTot'));
    app.userLoyalties=JSON.parse(localStorage.getItem('userLoyalties'));
    app.newLoyalties=JSON.parse(localStorage.getItem('newLoyalties'));
    app.successMessage = null;
    app.errorMessage = null;
    app.total=app.subtotal-app.userLoyalties*10;

    //sets the GUI according to the user's response to radio buttons
    app.setMethod = function (value) {
        if(value == "sampath"){
            app.sampath = true;
            app.dialog = false;
        }

        if(value == "dialog"){
            app.sampath = false;
            app.dialog = true;
        }
    };

    //validate and add ordering using service
    app.addOrdering = function (paymentDetails) {
        app.orderingData[0].username = $rootScope.user.data.username;

        if(app.validatePayment(paymentDetails)){

            paymentDetails.amount = app.total;
            if(paymentDetails.hasOwnProperty('holderName'))
                paymentDetails.method = "sampath";
            else if(paymentDetails.hasOwnProperty('phoneNumber'))
                paymentDetails.method = "dialog";

            //place the payment using checkout service
            Checkout.placePayment(paymentDetails).then(function (res,err) {
                    if(err){
                        if(paymentDetails.method =="dialog")
                        Checkout.placeDialogTransaction(paymentDetails);
                        else
                            Checkout.placeSampathTransaction(paymentDetails);
                    }
            });

            Checkout.placeOrdering(app.orderingData).then(function (res) {

                    app.successMessage="Ordering Added Successfully !";
                    app.errorMessage = null;
                    localStorage.removeItem('orderingData');

                    $scope.dialogPay = null;
                    $scope.sampathPay = null;
                    app.total = null;
                    $scope.paymentForm.$setPristine();
                    $scope.paymentForm.$setUntouched();
            })

        }else{
            app.errorMessage="Cannot Add ordering,please insert valid data";
            app.successMessage = null;
            }
    }

    //validate the payment credentials
    app.validatePayment = function (data) {
        if(data==null)
            return false;
        if(data.hasOwnProperty('holderName')){
            if(data.hasOwnProperty('ccNumber'))
                if(data.hasOwnProperty('cvcNumber'))
                    return true;
        }
        else if(data.hasOwnProperty('phoneNumber'))
            if(data.hasOwnProperty('pinNumber'))
                return true;
        else
            return false;
    }


}])