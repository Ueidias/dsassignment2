
angular.module('signUpController',[])

    .controller('signUpCtrl',['UserSer','$timeout','$location',function (UserSer,$timeout,$location) {
        const app = this;

        //signs up a new user by using the user service
        app.signupUser = function (signupdata) {
            signupdata.permission = "user";

            UserSer.signUp(signupdata).then(function (data) {
                if(data.data.success){
                    app.errorMessage = null;
                    app.successMessage = data.data.message+" Redirecting to login...";
                    $timeout(function () {
                        $location.path('/login');
                    },2000);
                }else {
                    app.errorMessage = data.data.message;
                }
            })
        }
    }])