
angular.module('foodController',[])

    .controller('foodCtrl',['food','$state','$stateParams','$rootScope','$scope',function (food,$state,$stateParams,$rootScope,$scope) {
        const app = this;

        app.mealmenu =[];
        app.foods = [];
        var finalTotal=0;
        var newLoyalties=0;
        app.successmsg="";
        app.randomUser="";
        app.orderings=[];
        app.ticketPrice=0;
        app.totalPrice=0;

        app.restaurants = [];
        app.resdetails = [];

        app.userLoyalties=0;
        app.loyaltyReductionPrice=0;
        app.loyaty=0;
        app.menuQty=[1,2,3,4,5];
        app.orderQty=0;
        app.date="";
        app.time="";
        app.menutimes=["4:00 pm","5:00 pm","6:00 pm","7:00 pm","8:00 pm","9:00 pm","10:00 pm","11:00 pm"]

        //Get random user
        food.getRandomUser().then(function (res) {
            app.randomUser = res.data;
        });

        food.getUserLoyalties($rootScope.user.data.username).then(function (res) {
            app.userLoyalties = res.data;
            console.log("[get user loyalties:]"+app.userLoyalties);
        });

        //Call service to get available menu item
        food.getAllmeals().then(function (res) {
            app.mealmenu = res.data;
        });

        //Call service to get a price of the selected meal
        app.updateloyalty=function(uId,data) {
            console.log(uId);
            console.log(data);
            food.updateLoyaltyPoints(uId,data).then(function (res) {
                    console.log(res);
            })
        };

        //Call service to get aprice of the selected meal
        app.getMealPriceNloyalty=function(foodId) {

            food.getMealprice(foodId).then(function (res) {
                app.Itemprice = res.data[0].mealPrice;
                app.loyalty = res.data[0].loyaltyPoints;
            })
        };

        //Calculate the total amount of the payment
        app.calculateTotal = function (val) {
            app.totalPrice += val;
        }

        //Validating and passing the ordering details to make the ordering
        app.addOrdering = function (ordering) {

            ordering.Itemprice = app.Itemprice;
            ordering.loyalty = app.loyalty;

            if(app.validateOrdering(ordering)){

                app.totalPrice=ordering.Itemprice*ordering.orderQty;

                ordering.totalPrice = app.totalPrice;
                app.orderings.push(ordering);

                finalTotal += app.totalPrice;
                newLoyalties += JSON.parse(ordering.loyalty);

                localStorage.setItem('finalTot',JSON.stringify(finalTotal));
                localStorage.setItem('userLoyalties',JSON.stringify(app.userLoyalties));
                localStorage.setItem('newLoyalties',JSON.stringify(newLoyalties));
                localStorage.setItem('orderingData',JSON.stringify(app.orderings));

                app.successmsg="You can add more items";
                app.errorMessage = null;
                app.orderings = JSON.parse(localStorage.getItem('orderingData'));

                var bodydata={
                    "loyaltyPoints":localStorage.getItem('newLoyalties')
                };
                app.updateloyalty($rootScope.user.data.username,bodydata);

                $state.transitionTo($state.current, $stateParams, {
                    reload: false, inherit: false, notify: true
                });
            }
            else{
                app.errorMessage = "Cannot proceed to checkout";
            }
        };

        //Validate the ordering form
        app.validateOrdering = function (ordering) {
            if(ordering == null) {
                console.log("ordering is null");
                return false;
            }
            if(ordering.hasOwnProperty('meal'))
                if(ordering.hasOwnProperty('Itemprice'))
                    if(ordering.hasOwnProperty('orderQty'))
                        if(ordering.hasOwnProperty('date'))
                            if(ordering.hasOwnProperty('time'))
                                if(ordering.hasOwnProperty('loyalty'))
                                    return true;
                                else {
                                    console.log("invalid ordering params");
                                    return false;
                                }
        };



    }]);