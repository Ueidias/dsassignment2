angular.module('userController',[])
    .controller('loginCtrl', ['Auth','$timeout','$location','$rootScope', function(Auth,$timeout,$location,$rootScope){
        const app = this;

        //validate and authenticate login
        app.validateLogin = function(loginData){
            Auth.login(loginData).then(function(data){
                if(data.data.success){
                    app.errorMessage = null;
                    app.successMessage = data.data.message+" Redirecting to menu selection...";
                    $timeout(function(){
                        Auth.getUser().then(function(data){
                            $rootScope.user = data;
                            if(data.data.permission=="user"){
                                $location.path('/dashboard/foods');
                            }
                        })
                    },2000);
                }else{
                    app.errorMessage = data.data.message;
                }
            })
        }

        //logout user from the client
        app.logout = function () {
            localStorage.removeItem('orderingData');
            localStorage.removeItem('finalTot');
            localStorage.removeItem('userLoyalties');
            localStorage.removeItem('newLoyalties');
            Auth.logout();
            $location.path('/home');
        }
    }])


