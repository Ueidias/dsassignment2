angular.module('userService',[])

.factory('UserSer',['$http',function ($http) {
    const userFactory = [];

    //signs up the user by posting given credentials
    userFactory.signUp = function(Data){
        return $http.post('http://localhost:4004/users', Data).then(function(data2){
            return data2;
        });
    }

    return userFactory;
}])