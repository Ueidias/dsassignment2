
angular.module('foodFactory',[])

    .factory('food',['$http',function ($http) {
        const foodFactory = [];

        //get random user
        foodFactory.getRandomUser = function () {
            return $http.get('http://localhost:5001/meals/userluck').then(function (data) {
                return data;
            })
        }

        //get all meal items
        foodFactory.getAllmeals = function () {
            return $http.get('http://localhost:5001/meals/menu').then(function (data) {
                return data;
            })
        }

        //get meal quantities available from database when meal item is given
        foodFactory.getMealprice = function (id) {
            return $http.get('http://localhost:5001/meals/'+id).then(function (data) {
                return data;
            })
        }

        // get loyalty points of users
        foodFactory.getUserLoyalties = function (Cusid) {
            return $http.get('http://localhost:5001/meals/customers/'+Cusid).then(function (data) {
                return data;
            })
        }

        //Save order details
        foodFactory.saveMealOrder = function (data) {
            return $http.post('http://localhost:5001/meals',data).then(function (data) {
                return data;
            })
        }

        //save the restaurant ordering
        foodFactory.updateLoyaltyPoints = function (id,data) {
            return $http.put('http://localhost:5001/meals/customers/'+id,data).then(function (data) {
                return data;
            })
        }
        return foodFactory;
    }])