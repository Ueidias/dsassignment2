angular.module('checkoutFactory',[])

    .factory('Checkout',['$http',function ($http) {

        var checkoutFactory = [];

        //place the ordering made by the user using a POST request
        checkoutFactory.placeOrdering = function (orderingData) {
            return $http.post('http://localhost:5001/meals',orderingData).then(function (data) {
                return data;
            })
        };

        //EI tooling
        //Note : Please change this IP according to IP of EI tooling
        checkoutFactory.placePayment = function (paymentDetails) {
            return $http.post('https://10.98.203.64:9443/services/transaction',paymentDetails).then(function (data,err) {
                return data;
            })
        };

        //these methods will be called in a occasion if EI tooling breaks
        checkoutFactory.placeDialogTransaction = function (orderingData) {
            return $http.post('http://localhost:5003/transaction',orderingData).then(function (data) {
                return data;
            })
        };
        checkoutFactory.placeSampathTransaction = function (orderingData) {
            return $http.post('http://localhost:5002/transaction',orderingData).then(function (data) {
                return data;
            })
        };
        return checkoutFactory;
    }]);