angular.module('authService',[])

    .factory('Auth',['$http','AuthToken','$q', function($http,AuthToken,$q){
        const authFactory = {};

        //user authentication
        authFactory.login = function(loginData){
            return $http.post('http://localhost:4004/authenticate', loginData).then(function(data){
                AuthToken.setToken(data.data.token);
                return data;
            });
        }

        //check whether the user is logged in
        //check whether the token exists in the local storage
        authFactory.isLoggedIn = function(){
            if(AuthToken.getToken()){
                return true;
            }else{
                return false;
            }
        }

        //logout user from the client
        authFactory.logout = function(){
            AuthToken.setToken();
        }

        //get user details from the database
        authFactory.getUser = function(){
            if(AuthToken.getToken()){
                return $http.get('http://localhost:4004/validate');
            }else{
                $q.reject({ message: 'User token not set'});
            }
        }

        return authFactory;
    }])

    //Set and remove the tokens
    .factory('AuthToken',['$window',function($window){
        const tokenFactory = {};
        //Storing token to the local storage
        tokenFactory.setToken = function(token){
            if(token){
                $window.localStorage.setItem('token',token);
            }else{
                $window.localStorage.removeItem('token');
            }
        }
        //get the user token from local storage
        tokenFactory.getToken = function(){
            return $window.localStorage.getItem('token');
        }
        return tokenFactory;
    }])


    .factory('AuthInterceptors', ['AuthToken', function(AuthToken){
        const authInterceptorsFactory = {};

        authInterceptorsFactory.request = function(req){
            const token = AuthToken.getToken();
            if(token){
                req.headers['x-access-token'] = token;
            }
            return req;
        }
        return authInterceptorsFactory;
    }])