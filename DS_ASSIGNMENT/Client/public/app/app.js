'use strict'

// Main application of the client
// this has all the dependencies of other modules
angular.module('foodClient',[
	'foodClient.routes',
	'userController',
	'foodController',
	'checkoutController',
	'foodFactory',
	'checkoutFactory',
    'authService',
    'userService',
    'signUpController'

])

    .config(function($httpProvider){
        $httpProvider.interceptors.push('AuthInterceptors');
    })

    .run( ['$rootScope','$location','Auth', function($rootScope, $location, Auth) {

        $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams){
            if(toState.authenticated==true){
                if(!Auth.isLoggedIn()){
                    event.preventDefault();
                    $location.path('/login');
                }else{
                    //get user data from the database
                    Auth.getUser().then(function(data){
                        if(data){
                            $rootScope.user = data;
                        }else{
                            $location.path('/login');
                        }
                    })
                    if(toState.permissions){
                        if($rootScope.user.data.permission != toState.permissions[0]){
                            $location.path('/dashboard/foods');
                        }
                    }
                }
            }else if (toState.authenticated==false){
                if(Auth.isLoggedIn()){
                    $location.path('/home');
                }
            }

        })
    }]);
