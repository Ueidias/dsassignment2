'use strict'


// setting all the routes of the Single page application client using AngularJS ui-router
angular.module('foodClient.routes',['ui.router'])
    .config(function($stateProvider, $urlRouterProvider) {

        $urlRouterProvider.otherwise('/home');
        $stateProvider

            .state('home', {
                url: '/home',
                templateUrl: 'app/views/pages/home.html',
                authenticated: false
            })

            .state('login',{
                url:'/login',
                templateUrl:'app/views/pages/user/login.html',
                controller:'loginCtrl as login',
                authenticated:false
            })

            .state('signup',{
                url:'/signup',
                templateUrl:'app/views/pages/user/signup.html',
                controller:'signUpCtrl as signup',
                authenticated:false
            })

            .state('dashboard',{
                url:'/dashboard',
                templateUrl:'app/views/pages/dashboard.html',
                controller:'loginCtrl as login',
                authenticated:true
            })

            .state('dashboard.food',{
                url:'/foods',
                templateUrl:'app/views/pages/ordering/food.html',
                controller:'foodCtrl as food',
                authenticated:true
            })

            .state('dashboard.checkout',{
                url:'/checkout',
                templateUrl:'app/views/pages/ordering/checkout.html',
                controller:'checkoutCtrl as checkout',
                authenticated:true
            })

            .state('dashboard.payment',{
                url:'/payment',
                templateUrl:'app/views/pages/ordering/payment.html',
                controller:'checkoutCtrl as checkout',
                authenticated:true
            })
    });