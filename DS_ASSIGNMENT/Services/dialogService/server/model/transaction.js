const mongoose = require('mongoose');

const transactionSchema = new mongoose.Schema({
    "phoneNumber":{type:String, unique:false, required:true},
    "pinNumber":{type:Number, unique:false, required:true},
    "amount":{type:Number, unique:false, required:true},
    "method":{type:String, unique:false, required:true}
});

var transaction = mongoose.model("Transaction",transactionSchema);

module.exports = transaction;