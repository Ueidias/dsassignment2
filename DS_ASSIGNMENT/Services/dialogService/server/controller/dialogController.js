const transaction = require('../model/transaction');

exports.setTransaction = function (req,res) {
    var trans = new transaction(req.body);
    console.log(req.body);

    //Save the transaction details received from the client
    trans.save(function (err,createdObject) {
        if(err){
            console.log(err);
        }
        res.json(createdObject);
    })
}