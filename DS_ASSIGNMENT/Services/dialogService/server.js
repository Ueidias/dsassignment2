const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const cors = require('cors');
const dialogController = require('./server/controller/dialogController')

const app = express();
app.use(express.static(__dirname+"/server"));
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json());
app.use(cors());
module.exports = app;

//connecting to the local mongodb instance
mongoose.connect("mongodb://localhost:27017/dialogService",function (err) {
    if(err){
        console.log(err);
    }
    console.log("Connected to mongodb");
})

const port = 5003;

//fire up the server
app.listen(port, function (err) {
    if(err){
        console.log(err);
    }
    console.log("App is listening on "+port);
})

app.post('/transaction',dialogController.setTransaction);// save the transaction