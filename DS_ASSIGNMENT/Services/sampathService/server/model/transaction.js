
const mongoose = require('mongoose');

const transactionSchema = new mongoose.Schema({
    "holderName":{type:String, unique:false, required:true},
    "ccNumber":{type:Number, unique:false, required:true},
    "cvcNumber":{type:Number, unique:false, required:true},
    "amount":{type:Number, unique:false, required:true},
    "method":{type:String, unique:false, required:true}
});

var transaction = mongoose.model("Transaction",transactionSchema);

module.exports = transaction;