const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const cors = require('cors');
const sampathController = require('./server/controller/sampathController')

const app = express();
app.use(express.static(__dirname+"/server"));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(cors());
module.exports = app;

//Connecting to the local mongodb instance
mongoose.connect("mongodb://localhost:27017/sampathBank",function (err) {
    if(err){
        console.log(err);
    }
    console.log("Connected to mongodb");
})

const port = 5002;

//Fire up the server
app.listen(port, function (err) {
    if(err){
        console.log(err);
    }
    console.log("App is listening on "+port);
})

app.post('/transaction',sampathController.setTransaction);