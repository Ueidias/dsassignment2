const mongoose = require('mongoose');


const mealSelectionSchema = new mongoose.Schema({
    "customerId":{type:String,requires:true},
    "mealId":{type:String,required:true},
    "date":{type:Date,required:true},
    "time":{type:String, required:true},
    "price":{type:Number,required:true},
    "loyaltyPoints":{type:Number}
});

var MealOrder = mongoose.model("MealOrder",mealSelectionSchema,'mealSelect');

module.exports = MealOrder;