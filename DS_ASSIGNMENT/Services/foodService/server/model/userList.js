const mongoose = require('mongoose');

const userList = new mongoose.Schema({
    "customerId":{type:String, required:true},
    "loyaltyPoints":{type:String, required:true}
});

const users = mongoose.model('userList',userList,'userlist');

module.exports = users;