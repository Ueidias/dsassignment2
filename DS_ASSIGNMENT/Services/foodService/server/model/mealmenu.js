const mongoose = require('mongoose');

const mealMenu = new mongoose.Schema({
    "mealName":{type:String, required:true},
    "mealPrice":{type:String, required:true},
    "loyaltyPoints":{type:String}
})

const menu = mongoose.model('menu',mealMenu,'mealmenu');

module.exports = menu;