const menu = require('../model/mealmenu');
const order =require('../model/selectMeal');
const users =require('../model/userList');

//get random user
exports.getRandomUser = function (req,res) {
    users.find().distinct('customerId',function (err,data) {
        if(err){
            console.log(err);
        }
        var numOfUsers=data.length-1;
        var randomUsr=Math.floor((Math.random() * numOfUsers) + 1);
        res.json(data[randomUsr]);
    });
};

//get all meal items
exports.getAllmeals = function (req,res) {
    menu.find().distinct('mealName',function (err,data) {
        if(err){
            console.log(err);
        }
         res.json(data)
    });
};

//get meal quantities available from database when meal item is given
exports.getMealprice = function (req,res) {
    var tId = req.params.id;
    menu.find({'mealName':tId},function (err,data) {
        if(err){
            console.log(err);
        }
        res.json(data)
    });
};


// get loyalty points of users
exports.getUserLoyalties = function(req,res){
    users.find({customerId:req.params.id},function (err,data) {
        if(err){
            console.log(err);
        }
        res.json(data[0].loyaltyPoints);
    })
};

//save the meal ordering to the database
exports.saveMealOrder = function (req,res) {
   // var mealOrder = new order(req.body);

    var mealOrder = new order({
        customerId: req.body.username,
        mealId: req.body.meal,
        date: req.body.date,
        time: req.body.time,
        price: req.body.username,
        loyaltyPoints: req.body.userLoyalties
    });
    mealOrder.save(function (err,data) {
        if(err){
            var msg = {
                "message":"error"
            }
            res.send(msg);
            return;
        }
        var msg = {
            "message":"success"
        }
        res.send(msg);
    })
};
//update the Loyalty points of the customer
exports.updateLoyaltyPoints = function (req,res) {
    users.update({customerId:req.params.id},req.body,function (err,data) {
        if(err){
            var msg = {
                "message":"error"
            }
            res.send(msg);
            return;
        }
        var msg = {
            "message":"success"
        }
        res.send(msg);
    })
};