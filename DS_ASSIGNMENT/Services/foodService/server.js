var express = require('express');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var mealController = require('./server/controller/mealController');
var cors = require('cors');

var app = express();
const port = 5001;
app.use(cors());
app.use(express.static(__dirname+"/server"));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

//Connecting to local mongodb instance
mongoose.connect('mongodb://localhost:27017/foodordering',function (err) {
    if(err){
        console.log(err);
        process.exit(1);
    }
    console.log("Connected to mongodb");
})

//firing up the server
app.listen(port,function (err) {
    if(err){
        console.log(err);
    }
    console.log("Food service is listening on "+port);
})
app.get("/meals/userluck",mealController.getRandomUser);
app.get("/meals/menu",mealController.getAllmeals);
app.get("/meals/:id",mealController.getMealprice);
app.get("/meals/customers/:id",mealController.getUserLoyalties);
app.post("/meals",mealController.saveMealOrder);
app.put("/meals/customers/:id",mealController.updateLoyaltyPoints);
