var express = require('express');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var userController = require('./server/controller/userController');
var cors = require('cors');

var app = express();
const port = 4004;
app.use(cors());
app.use(express.static(__dirname+"/server"));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));


//Connecting to local mongodb instance
mongoose.connect('mongodb://localhost:27017/userdb',function (err) {
    if(err){
        console.log(err);
        process.exit(1);
    }
    console.log("Connected to mongodb");
})

//Firing up the user service (Authentication server)
app.listen(port,function (err) {
    if(err){
        console.log(err);
    }
    console.log("user service is listening on "+port);
})


app.post('/users',userController.addNewUser); //Adds new user
app.post('/authenticate', userController.authenticateUser); //Authenticate the login
app.get('/validate', userController.validateMe);//Validate the token