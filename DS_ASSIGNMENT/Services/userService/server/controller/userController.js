const User = require('../model/user');
const webtoken = require('jsonwebtoken');
const secret = '#f0odMagic!';

//Add new user to the database
exports.addNewUser = function(req,res){
    const user = new User();
    user.username = req.body.username;
    user.password = req.body.password;
    user.email = req.body.email;
    user.permission = req.body.permission;
    user.save(function(err){
        if(req.body.username==null || req.body.username==""){
            res.status(406).json({ success:false, message:'Username value not set'});
        }else if (req.body.password==null || req.body.password==""){
            res.status(406).json({ success:false, message: 'Password value not set'});
        }else if (req.body.email==null || req.body.email==""){
            res.status(406).json({ success:false, message:'Email value not set'});
        }else{
            if(err){
                res.status(406).json(err);
            }else{
                res.status(201).json({ success:true, message: 'New user created!'});
            }
        }
    });
};


exports.authenticateUser = function(req,res){
    User.findOne({ username: req.body.username }).select('username password email name permission').exec(function(err,user){
        if(err) throw err;
        if(!user){
            res.json({ success:false, message: 'Could not authenticate user!'});
        }else if(user){
            //validate the password
            const validPassword = user.comparePassword(req.body.password);
            if(!validPassword){
                res.json({ success:false, message:'Could not validate user!' });
            }else{
                //set the json web token
                const token = webtoken.sign({
                    username: user.username,
                    name: user.name,
                    email: user.email,
                    permission: user.permission
                },secret, { expiresIn: '1h'});
                res.json({ success:true, message:'Authenticated !', token:token});
            }
        }
    })
}

//get user details and validate the token
exports.validateMe = function(req,res){
    const token = req.body.token || req.body.query || req.headers['x-access-token'];

    if(token){
        webtoken.verify(token, secret, function(err, decoded){
            if(err) {
                res.json({ success:false, message: 'Token invalid'});
            }else{
                res.send(decoded);
            }
        })
    }else{
        res.json({ success:false, message: 'Token not set'});
    }
}
